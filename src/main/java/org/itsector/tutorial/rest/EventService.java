package org.itsector.tutorial.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import org.itsector.tutorial.model.Event;
import org.jboss.as.ee.component.EnvEntryInjectionSource;

@Path("/event")
@Stateless
public class EventService extends BaseEntityService<Event> {

	public EventService() {
		super(Event.class);
	}

	@Override
	protected Predicate[] extractPredicates(
			MultivaluedMap<String, String> queryParameters,
			CriteriaBuilder criteriaBuilder, Root<Event> root) {
		
		List<Predicate> predicates = new ArrayList<Predicate>();
		
		if(queryParameters.containsKey("category")){
			String category = queryParameters.getFirst("category");
			predicates.add(criteriaBuilder.equal(root.get("category").get("id"), category));
		}
		
		return predicates.toArray(new Predicate[]{});
	}

}
