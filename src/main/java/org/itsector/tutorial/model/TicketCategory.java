package org.itsector.tutorial.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * <p>
 * A lookup table containing the various ticket categories. E.g. Adult, Child,
 * Pensioner, etc.
 * </p>
 * 
 * @author nuno.neves@itsector.pt
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "TICKET_CATEGORY")
public class TicketCategory implements Serializable {

	/* Declaration of fields */

	/**
	 * The synthetic id of the object
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/**
	 * <p>
	 * The description o the of ticket category.
	 * </p>
	 * 
	 * <p>
	 * The description forms the natural id of the ticket category, and so must
	 * be unique.
	 * </p>
	 */
	@Column(unique = true)
	@NotEmpty
	// This is Bean Validation (JSR 303)
	private String description;

	/* Boilerplate getters and setters */

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/*
	 * toString(), equals() and hashCode() for TicketCategory, using the naturl
	 * identity of the object
	 */

	@Override
	public String toString() {
		return "TicketCategory [description=" + description + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TicketCategory other = (TicketCategory) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		return true;
	}

}
