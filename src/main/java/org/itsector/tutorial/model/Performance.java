package org.itsector.tutorial.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * A performance represents a single instance of a show.
 * </p>
 * 
 * <p>
 * The show and date form the natural id of this entity, and therefore must be
 * unique. JPA requires us to use the class level <code>@Table</code>
 * constraint.
 * </p>
 * 
 */
@Entity
@Table(name = "PERFORMANCE", uniqueConstraints = @UniqueConstraint(columnNames = {
		"date", "show_id" }))
public class Performance {

	/* Declaration of fields */

	/**
	 * The synthetic id of the object.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/**
	 * <p>
	 * The date and start time of the performance.
	 * </p>
	 * 
	 * <p>
	 * A Java {@link Date} object represents both a date and a time, whilst an
	 * RDBMS splits out Date, Time and Timestamp. Therefore we instruct JPA to
	 * store this date as a timestamp using the
	 * <code>@Temporal(TIMESTAMP)</code> annotation.
	 * </p>
	 * 
	 * <p>
	 * The date and time of the performance is required, and the Bean Validation
	 * constraint <code>@NotNull</code> enforces this.
	 * </p>
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@NotNull
	private Date date;

	/**
	 * <p>
	 * The show of which this is a performance. The
	 * <code>@ManyToOne<code> JPA mapping
	establishes this relationship.
	 * </p>
	 * 
	 * <p>
	 * The show of which this is a performance is required, and the Bean
	 * Validation constraint <code>@NotNull</code> enforces this.
	 * </p>
	 */
	@ManyToOne
	@NotNull
	private Show show;

	/* Boilerplate getters and setters */

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Show getShow() {
		return show;
	}

	public void setShow(Show show) {
		this.show = show;
	}

	/*
	 * equals() and hashCode() for Performance, using the natural identity of
	 * the object
	 */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((show == null) ? 0 : show.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Performance other = (Performance) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (show == null) {
			if (other.show != null)
				return false;
		} else if (!show.equals(other.show))
			return false;
		return true;
	}
}
