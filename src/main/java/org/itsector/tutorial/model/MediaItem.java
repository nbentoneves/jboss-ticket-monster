package org.itsector.tutorial.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.URL;

/**
 * <p>
 * A reference to a media object such as images, sound bites, video recordings,
 * that can be used in the application.
 * </p>
 * 
 * <p>
 * A media item contains the type of the media, which is required to render it
 * correctly, as well as the URL at which the media should be sourced.
 * </p>
 * 
 * @author Marius Bogoevici
 * @author Pete Muir
 */
@Entity
@Table(name = "MEDIAITEM")
public class MediaItem {

	/* Declaration of fields */

	/**
	 * The synthetic id of the object.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/**
	 * <p>
	 * The type of the media, required to render the media item correctly.
	 * </p>
	 * <p>
	 * The media type is a <em>closed set</em> - as each different type of media
	 * requires support coded into the view layers, it cannot be expanded upon
	 * without rebuilding the application. It is therefore represented by an
	 * enumeration. We instruct JPA to store the enum value using it's String
	 * representation, so that we can later reorder the enum members, without
	 * changing the data. Of course, this does mean we can't change the names of
	 * media items once the app is put into production.
	 * </p>
	 */
	@Enumerated(EnumType.STRING)
	private MediaType mediaType;

	/**
	 * <p>
	 * The URL from which the media item can be sourced
	 * </p>
	 * 
	 * <p>
	 * The url of the media item forms it's natural id and cannot be shared
	 * between event categories
	 * </p>
	 * 
	 * <p>
	 * The
	 * <code>@URL<code> Bean Validation ensures the the URL is, indeed, a valid URL.
	 * </p>
	 */
	@Column(unique = true)
	@URL
	private String url;

	/* Boilerplate getters and setters */

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public MediaType getMediaType() {
		return mediaType;
	}

	public void setMediaType(MediaType mediaType) {
		this.mediaType = mediaType;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	/*
	 * toString(), equals() and hashCode() for MediaItem, using the natural
	 * identity of the object
	 */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MediaItem other = (MediaItem) obj;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MediaItem [" + mediaType.getDescription() + "] " + url;
	}

}
