package org.itsector.tutorial.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@SuppressWarnings("serial")
@Entity
@Table(name = "EVENT")
// @SequenceGenerator(name="Event_SEQ", initialValue=1, allocationSize=100)
public class Event implements Serializable {

	/* Declaration of fields */

	/**
	 * The synthetic ID of the object.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	// @Column(name = "id", updatable = false, nullable = false)
	private Long id = null;

	/**
	 * <p>
	 * The name of the event.
	 * </p>
	 * 
	 * <p>
	 * The name of the event forms it's natural identity and cannot be shared
	 * between events.
	 * </p>
	 * 
	 * Ticket Monster Tutorial 119 / 293
	 * <p>
	 * Two constraints are applied using Bean Validation
	 * </p>
	 * 
	 * <ol>
	 * <li><code>@NotNull</code> &mdash; the name must not be null.</li>
	 * <li><code>@Size</code> &mdash; the name must be at least 5 characters and
	 * no more than 50 characters. This allows for better formatting consistency
	 * in the view layer.</li>
	 * </ol>
	 */
	@Column(length = 255)
	@NotNull
	@Size(message = "Must be > 5 and < 50", min = 5, max = 50)
	private String name;

	/**
	 * <p>
	 * A description of the event.
	 * </p>
	 * 
	 * <p>
	 * Two constraints are applied using Bean Validation
	 * </p>
	 * 
	 * <ol>
	 * <li><code>@NotNull</code> &mdash; the description must not be null.</li>
	 * <li><code>@Size</code> &mdash; the name must be at least 20 characters
	 * and no more than 1000 characters. This allows for better formatting
	 * consistency in the view layer, and also ensures that event organisers
	 * provide at least some description - a classic example of a business
	 * constraint.</li>
	 * </ol>
	 */
	@Column
	@Size(message = "An event's name must contain between 20 and 1000 characters", min = 20, max = 1000)
	private String description;

	/**
	 * <p>
	 * A media item, such as an image, which can be used to entice a browser to
	 * book a ticket.
	 * </p>
	 * 
	 * <p>
	 * Media items can be shared between events, so this is modeled as a
	 * <code>@ManyToOne</code> relationship.
	 * </p>
	 * Ticket Monster Tutorial 121 / 293
	 * 
	 * <p>
	 * Adding a media item is optional, and the view layer will adapt if none is
	 * provided.
	 * </p>
	 * 
	 */
	@ManyToOne
	private MediaItem mediaItem;

	/**
	 * <p>
	 * The category of the event
	 * </p>
	 * 
	 * <p>
	 * Event categories are used to ease searching of available of events, and
	 * hence this is modeled as a relationship
	 * </p>
	 * 
	 * <p>
	 * The Bean Validation constraint <code>@NotNull</code> indicates that the
	 * event category must be specified.
	 */
	@ManyToOne
	@NotNull
	private EventCategory eventCategory;

	/* Boilerplate getters and setters */

	public Long getId() {
		return this.id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public MediaItem getMediaItem() {
		return mediaItem;
	}

	public void setMediaItem(MediaItem mediaItem) {
		this.mediaItem = mediaItem;
	}

	public EventCategory getEventCategory() {
		return eventCategory;
	}

	public void setEventCategory(EventCategory eventCategory) {
		this.eventCategory = eventCategory;
	}

	/*
	 * toString(), equals(), and hashCode() for Event, using the natural
	 * identity of the object
	 */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Event other = (Event) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		String result = getClass().getSimpleName() + " ";
		if (name != null && !name.trim().isEmpty())
			result += "name: " + name;
		if (description != null && !description.trim().isEmpty())
			result += ", description: " + description;
		return result;
	}
}