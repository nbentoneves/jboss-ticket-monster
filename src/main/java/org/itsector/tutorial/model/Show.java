package org.itsector.tutorial.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * A show is an instance of an event taking place at a particular venue. A show
 * can have multiple performances.
 * </p>
 */
@Entity
@Table(name = "SHOW", uniqueConstraints = @UniqueConstraint(columnNames = {
		"event_id", "venue_id" }))
public class Show {

	/* Declaration of fields */

	/**
	 * The Synthetic id of the object.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/**
	 * <p>
	 * The event of which this show is an instance. The
	 * <code>@ManyToOne<code> JPA mapping
	establishes this relationship.
	 * </p>
	 * <p>
	 * The <code>@NotNull</code> Bean Validation constraint means that the event
	 * must be specified.
	 * </p>
	 */
	@ManyToOne
	@NotNull
	private Event event;

	/**
	 * <p>
	 * The venue where this show takes place. The
	 * <code>@ManyToOne<code> JPA mapping
	establishes this relationship.
	 * </p>
	 * 
	 * <p>
	 * The <code>@NotNull</code> Bean Validation constraint means that the venue
	 * must be specified.
	 * </p>
	 */
	@ManyToOne
	@NotNull
	private Venue venue;

	/**
	 * <p>
	 * The set of performances of this show.
	 * </p>
	 * 
	 * <p>
	 * The
	 * <code>@OneToMany<code> JPA mapping establishes this relationship. Collection
	members
	* are fetched eagerly, so that they can be accessed even after the entity has become
	detached.
	* This relationship is bi-directional (a performance knows which show it is part of),
	and the <code>mappedBy</code> attribute establishes this.
	 * </p>
	 * 
	 */
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "show")
	@OrderBy("date")
	private Set<Performance> performances = new HashSet<Performance>();

	/* Boilerplat getters and setters */

	public Long getId() {
		return id;
	}

	public Event getEvent() {
		return event;
	}

	public Venue getVenue() {
		return venue;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public void setVenue(Venue venue) {
		this.venue = venue;
	}

	public Set<Performance> getPerformances() {
		return performances;
	}

	public void setPerformances(Set<Performance> performances) {
		this.performances = performances;
	}

	/*
	 * toString(), equals() and hashCode() for show, using the natural identity
	 * of the object
	 */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((event == null) ? 0 : event.hashCode());
		result = prime * result + ((venue == null) ? 0 : venue.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Show other = (Show) obj;
		if (event == null) {
			if (other.event != null)
				return false;
		} else if (!event.equals(other.event))
			return false;
		if (venue == null) {
			if (other.venue != null)
				return false;
		} else if (!venue.equals(other.venue))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return event + " at " + venue;
	}

}
