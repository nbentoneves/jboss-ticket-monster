package org.itsector.tutorial.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(EventCategory.class)
public abstract class EventCategory_ {

	public static volatile SingularAttribute<EventCategory, Long> id;
	public static volatile SingularAttribute<EventCategory, String> description;

}

